﻿using System.Collections.Generic;

namespace WebApplication1.DAL.Entities
{
    public class Author : BaseEntity
    {
        public string FirstName { get; set; }
        public string Surname { get; set; }

        public ICollection<Book> Books { get; set; }
    }
}
