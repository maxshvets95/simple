﻿namespace WebApplication1.DAL.Entities
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }
    }
}
