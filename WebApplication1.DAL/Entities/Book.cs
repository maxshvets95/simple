﻿namespace WebApplication1.DAL.Entities
{
    public class Book : BaseEntity
    {
        public string Name { get; set; }
        public int Year { get; set; }
        public decimal Price { get; set; }

        public int AuthorId { get; set; }
        public Author Author { get; set; }
    }
}
