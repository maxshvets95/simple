﻿using System.Data.Entity;
using WebApplication1.DAL.Entities;

namespace WebApplication1.DAL.EF
{
    public class BookContext : DbContext
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<Author> Authors { get; set; }

        static BookContext()
        {
            Database.SetInitializer(new StoreDbInitializer());
        }
    }

    public class StoreDbInitializer : DropCreateDatabaseIfModelChanges<BookContext>
    {
        protected override void Seed(BookContext db)
        {
            db.Authors.Add(new Author { FirstName = "Лев", Surname = "Толстой" });
            db.Authors.Add(new Author { FirstName = "Александр", Surname = "Пушкин" });

            db.SaveChanges();
        }
    }
}
