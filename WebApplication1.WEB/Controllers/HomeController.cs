﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.BLL.DTO;
using WebApplication1.BLL.Interfaces;
using WebApplication1.BLL.Services;
using WebApplication1.WEB.Models;

namespace WebApplication1.WEB.Controllers
{
    public class HomeController : Controller
    {
        IOrderService orderSerice;

        public HomeController(IOrderService serv)
        {
            orderSerice = serv;
        }

        public ActionResult Index()
        {
            IEnumerable<AuthorDTO> authorDTOs = orderSerice.GetAuthors();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<AuthorDTO, AuthorViewModel>()).CreateMapper();
            var authors = mapper.Map<IEnumerable<AuthorDTO>, IEnumerable<AuthorViewModel>>(authorDTOs);
            return View(authors);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}