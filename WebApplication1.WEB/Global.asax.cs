﻿using SimpleInjector.Integration.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using SimpleInjector;
using SimpleInjector.Integration.Web.Mvc;
using WebApplication1.BLL.Interfaces;
using WebApplication1.BLL.Services;
using WebApplication1.DAL.EF;
using System.Data.Entity;
using WebApplication1.DAL.Interfaces;
using WebApplication1.DAL.Repositories;
using WebApplication1.DAL.Entities;
using WebApplication1.WEB.Util;
using WebApplication1.BLL.Util;

namespace WebApplication1.WEB
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // Create the container as usual.
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

            // Register your types, for instance:
            container.Register<IOrderService, OrderService>(Lifestyle.Scoped);
            container.Register<IGenericRepository<Author>, EFGenericRepository<Author>>(Lifestyle.Scoped);

            container.Register<DbContext, BookContext>(Lifestyle.Scoped);

            container.Verify();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));

            AutoMapperBllConfiguration.Configure();
            AutoMapperPLConfiguration.Configure();
        }
    }
}
