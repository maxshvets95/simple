﻿using AutoMapper;
using WebApplication1.BLL.DTO;
using WebApplication1.WEB.Models;

namespace WebApplication1.WEB.Util
{
    public class AutoMapperPLConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<AuthorViewModel, AuthorDTO>().ReverseMap();
            });
        }
    }
}