﻿ using AutoMapper;
using WebApplication1.BLL.DTO;
using WebApplication1.DAL.Entities;

namespace WebApplication1.BLL.Util
{
    public class AutoMapperBllConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<AuthorDTO, Author>().ReverseMap();
            });
        }
    }
}
