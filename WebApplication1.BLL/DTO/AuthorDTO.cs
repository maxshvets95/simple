﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApplication1.BLL.DTO
{
    public class AuthorDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
    }
}
