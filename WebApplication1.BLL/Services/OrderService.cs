﻿using AutoMapper;
using System.Collections.Generic;
using WebApplication1.BLL.DTO;
using WebApplication1.BLL.Interfaces;
using WebApplication1.DAL.Entities;
using WebApplication1.DAL.Interfaces;

namespace WebApplication1.BLL.Services
{
    public class OrderService : IOrderService
    {
        private IGenericRepository<Author> _repository;

        public OrderService(IGenericRepository<Author> repository)
        {
            _repository = repository;
        }
        public IEnumerable<AuthorDTO> GetAuthors()
        {
            var authors = _repository.Get();
            var result = Mapper.Map<IEnumerable<Author>, IEnumerable<AuthorDTO>>(authors);

            return result;
        }
    }
}
