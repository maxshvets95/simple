﻿using System.Collections.Generic;
using WebApplication1.BLL.DTO;
using WebApplication1.DAL.Entities;

namespace WebApplication1.BLL.Interfaces
{
    public interface IOrderService
    {
        IEnumerable<AuthorDTO> GetAuthors();
    }
}
